#ifndef EMU_H
#define EMU_H

#include <windows.h>

#define EMU_MICKEYS_PER_CM 314.960630
#define EMU_BASE_DPI 800.0
#define EMU_DELAY    50
#define EMU_MATCH_FULL   0
#define EMU_MATCH_HALF   1
#define EMU_MATCH_MANUAL 2
#define EMU_LINE_BREAK "\n----------\n"
#define EMU_MOUSE_MOVE "Moving Mouse: %f FOV at %f CM/360, %f DPI, %f parts.\n"
#define EMU_DEGREE_MOVE "\nMoved %f degrees. Total of %f CMs.\n"

enum emu_direction {
        HORIZONTAL,
        VERTICAL
};

enum emu_key_state {
        INACTIVE,
        RELEASED,
        PRESSED
};

/* Settings */
typedef struct
{
        double cm360;
        double dpi;
        double current_fov;
        double new_fov;
        double helper_fov;
        double parts;
        double match_value;
        double sensitivity;
        double unit_counter;
        double static_at_one;
        int match_mode;
        int to_fov;
        int calc_movement;
        int helper_mode;
        int error_flag;
    
} emu_settings;

void EmuPrintSettings(emu_settings s);

/* Simulate mouse movements in Mickeys */
void EmuMoveMouse(int x, int y); 

/* Move to Horizontal FOV at a given CM/360 and dpi */
void EmuToFOV(enum emu_direction d, double cm, double dpi, double fov, 
              double parts, double max_parts);

/* Calculate and move mouse to a given CM/360 */
void EmuCalcMovement(enum emu_direction d, int key, double cm, int dpi, 
                     double parts, double *unit_count, double max_parts);

/* Calculate and move mouse to a given CM/360 */
void EmuCalcVMovement(int key, double cm, int dpi, double parts, 
                      double *unit_count, double max_parts);

int EmuKeyPress(int key, int wait);

DWORD WINAPI EmuMain(void *data);

#endif
