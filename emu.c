#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include "emu.h"

void EmuPrintSettings(emu_settings s)
{
        printf("\nSettings"
               EMU_LINE_BREAK
               "CM/360: %f\n"
               "DPI: %f\n"
               "Current FOV: %f\n"
               "New FOV: %f\n"
               "Helper FOV: %f\n"
               "Parts: %f\n"
               "Match Value: %f\n"
               "Sensitivity: %f\n"
               "Static At One: %f\n"
               "Match Mode: %i"
               EMU_LINE_BREAK, s.cm360, s.dpi, s.current_fov, s.new_fov, 
               s.helper_fov, s.parts, s.match_value, s.sensitivity, 
               s.static_at_one, s.match_mode);
}

void EmuMoveMouse(int x, int y)
{
        INPUT mouse_input;

        mouse_input.type  = INPUT_MOUSE;
        mouse_input.mi.dx = x;
        mouse_input.mi.dy = y;
        mouse_input.mi.time        = 0;
        mouse_input.mi.dwExtraInfo = 0;
        mouse_input.mi.mouseData   = 0;
        mouse_input.mi.dwFlags     = MOUSEEVENTF_MOVE;

        SendInput(1, &mouse_input, sizeof(mouse_input));
}

void EmuToFOV(enum emu_direction d, double cm, double dpi, double fov, 
              double parts, double max_parts)
{
        if (parts > max_parts) parts = max_parts;

        printf(EMU_LINE_BREAK EMU_MOUSE_MOVE, fov, cm, dpi, parts);
    
        double dpi_factor     = dpi / EMU_BASE_DPI;
        double total_cm_count = cm * EMU_MICKEYS_PER_CM * dpi_factor;
        double total_units    = (total_cm_count / 360) * fov;
        double unit_count = 0;
        double fov_count  = 0;
        double cm_count   = 0;
    
        while (total_units > parts) {\
                if (d == HORIZONTAL)
                        EmuMoveMouse(parts, 0);
                else
                        EmuMoveMouse(0, parts);

                total_units -= parts;
                unit_count  += parts;
                fov_count = unit_count / (total_cm_count / 360);
                cm_count  = (unit_count / dpi_factor) / EMU_MICKEYS_PER_CM;

                printf(EMU_DEGREE_MOVE, fov_count, cm_count);
                Sleep(EMU_DELAY);
        }

        EmuMoveMouse(total_units, 0);
        unit_count += total_units;
        fov_count  = unit_count / (total_cm_count / 360);
        cm_count   = (unit_count / dpi_factor) / EMU_MICKEYS_PER_CM;

        printf(EMU_DEGREE_MOVE EMU_LINE_BREAK, 
               fov_count, cm_count);
}

void EmuCalcMovement(enum emu_direction d, int key, double cm, int dpi, 
                     double parts, double *unit_count, double max_parts)
{
        if (parts > max_parts) 
                parts = max_parts;
    
        double dpi_factor     = dpi / EMU_BASE_DPI;
        double total_cm_count = cm * EMU_MICKEYS_PER_CM * dpi_factor;
        double fov_count = 0;
        double cm_count  = 0;
    
        while (GetKeyState(key) & 0x8000) {
                if (d == HORIZONTAL)
                        EmuMoveMouse(parts, 0);
                else
                        EmuMoveMouse(0, parts);

                *unit_count += parts;
                fov_count   = *unit_count / (total_cm_count / 360);
                cm_count    = (*unit_count / dpi_factor) / EMU_MICKEYS_PER_CM;

                printf(EMU_DEGREE_MOVE, fov_count, cm_count);
                Sleep(EMU_DELAY);
        }
}

int EmuKeyPress(int key, int wait)
{
        enum emu_key_state state = INACTIVE;

        if(GetKeyState(key) & 0x8000) {
                while((GetKeyState(key) & 0x8000) && wait == 1) 
                        Sleep(50);

                state = RELEASED;
                return state;
        }

        return state;
}

double EmuGetMatchFov(double fov, int mode)
{
        if (mode == EMU_MATCH_FULL)
                return 360.0;

        if (mode == EMU_MATCH_HALF)
                return fov / 2;

        return fov;
}

DWORD WINAPI EmuMain(void *data)
{
        emu_settings *s = calloc(1, sizeof(emu_settings));

        if (s == NULL)
                exit(EXIT_FAILURE);

        s = data;

        int desktop_width = GetSystemMetrics(SM_CXSCREEN);
        double max_parts  = ((double)desktop_width / 2.0);
    
        while (1) {
                if(EmuKeyPress(s->to_fov, 1)) {
                        double fov = s->helper_fov;

                        if (s->match_mode < EMU_MATCH_MANUAL)
                                fov = EmuGetMatchFov(s->current_fov, 
                                                     s->match_mode);

                        if(s->helper_mode) 
                                EmuToFOV(VERTICAL, s->cm360, s->dpi, fov,
                                         s->parts, max_parts);

                        else
                                EmuToFOV(HORIZONTAL, s->cm360, s->dpi, fov, 
                                         s->parts, max_parts);
                }

                if(EmuKeyPress(s->calc_movement, 0))
                {
                        if(s->helper_mode)
                                EmuCalcMovement(VERTICAL, s->calc_movement, 
                                                s->cm360, s->dpi, s->parts, 
                                                &s->unit_counter, max_parts);
                        else
                                EmuCalcMovement(HORIZONTAL, s->calc_movement,
                                                s->cm360, s->dpi, s->parts, 
                                                &s->unit_counter, max_parts);
                }

                Sleep(50);
        }

        return EXIT_SUCCESS;
}
